# Keep grandparent computer clean

Ceci est un tutoriel destiné aux grand-parents afin qu'ils nettoient eux-même leur ordinateur

## Hébergement

Le site est disponible [à cette adresse](https://nicolasbfr.gitlab.io/keep-grandparent-computer-clean), et est hébergé par Gitlab Pages.

## Licence

Le projet est sous [Licence MIT](LICENSE).