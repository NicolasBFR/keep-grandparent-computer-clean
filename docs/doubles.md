# Trouver les fichiers en double

Il est parfois compliqué d'organiser les photos/documents de son ordinateur. Heureusement, CCleaner est pourvu d'un détecteur de fichiers dupliqués.

## Accès

Allumez CCleaner, puis allez dans `Outils` puis `Recherche de fichiers en double`.

## Configuration

Les fichiers dupliqués peuvent avoir des noms et des dates différentes. Décochez donc tout dans `Correspondance par` et rechochez `Contenu` - `Taille` se recoche naturellement.

Il faut également choisir les dossiers à analyser. Il est inutile de chercher les fichiers des programmes qui ont naturellement des fichiers dupliqués. On ne veut que les dossiers utilisateur.

Dans `Inclure`, décochez donc toutes les options, puis cliquez sur `Ajouter`.

Dans `Options`, demandez à inclure les fichers et les sous-fichiers, et pour `Disque ou dossier`, choisissez votre dossier utilisateur en utilisant `Parcourir...` . Confirmez ensuite avec `OK`.

Si vous avez d'autres dossiers sur un autre disque, vous pouvez les ajouter de la même manière.

## Recherche

Cliquez en bas sur `Rechercher`, puis patientez (cela peut être long).

Une interface s'affiche, avec des groupes de fichiers dupliqués, leurs noms et leur emplacements sont affichés. Vous ne pouvez pas supprimer toutes les occurences d'un fichier. 

Vous pouvez cliquer sur `Supprimer la sélection` pour supprimer les fichiers cochés sans avoir à relancer la recherche ensuite.

Si vous constater qu'un dossier entier est dupliqué, faites clique droit sur un fichier, et cliquez sur l'option permettant d'ouvrir le dossier contenant. Vous pouvez ensuite supprimer le dossier - mais préférez d'abord essayer de transférer les fichiers du dossier à supprimer dans le dossier contenant les duplicata, et cliquez sur `Ignorer` et cochez la case `Faire pour tous les fichiers` quand l'ordinateur que faire que les fichiers sont identiques. Si vous avez peur de perdre des fichiers dans le processus, faites le uniquement par l'interface CCleaner.