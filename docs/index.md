# Nettoyer son ordinateur

L'ordinateur est un puissant outil, hélas souvent lent et grippé à cause des programmes installés qui n'hésitent pas à s'arroger toutes les permissions et toute la place, avec pour résultat des minutes entières de chargement. Voici des solutions de base pour accélérer son ordinateur.

## CCleaner
`Ccleaner` dispose d'outils importants pour le nettoyage de l'ordinateur et est [disponible ici](https://www.ccleaner.com/fr-fr/ccleaner/download).
### Nettoyeur
La section `Nettoyeur`  permet de rendre plus rapides les navigateurs Web.

Il suffit de cliquer sur `Analyser`, puis, une fois l'analyse terminée, de cliquer sur `Nettoyer`

### Registre
Permet de retirer des "déclarations" inutiles ou érronées de la part de logiciels à l'ordinateur.

Allez dans `Registre`, puis `Cherchez des erreurs` et enfin `Corriger les erreurs`.

Il demande une sauvegarde, vous pouvez accepter en la mettant dans un dossier créé pour l'occasion <u>avec un nom explicite</u> (comme sauvegardeRegistre). Corrigez ensuite les erreurs.

### Démarrage

La sous-section `Démarrage`, de la section `Outils` permet d'empêcher des programmes inutiles de s'allumer en arrière-plan et de ralentir l'ordinateur.

Allez dans les deux premiers onglets, `Windows` et `Tâches planifiées` et retirer les programmes dont vous n'avez pas besoin en permanence - comme CCleaner qui se lance au démarrage sans raison, et les mises à jour de navigateur et autre.

## Malwarebytes

`Malwarebytes` est un antivirus pour ordinateur [disponible ici](https://fr.malwarebytes.com/mwb-download/).

Lancez chaque mois un scan de vos fichiers pour éradiquer les virus.

## Améliorer son navigateur

`Google Chrome` a tendance à être lourd et à consommer la puissance de l'ordinateur. Vous pouvez tenter d'y installer l'extension [Privacy badger](https://privacybadger.org/) - également disponible sur `Firefox`.

## Lire ses mails depuis Thunderbird
`Thunderbird` est un client messagerie qui permet de lire ses mails bien plus rapidement que depuis un navigateur, ce qui, à la longue, est un énorme gain de temps. Il est disponible [à ce lien](https://www.thunderbird.net/fr/).