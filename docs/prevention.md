# Prévenir les piratages

## Conseils de base
Pour éviter les piratages, vous pouvez d'abord suivre les conseils de la page principale de ce site, particulièrement les trois derniers.
`Malware-Bytes` et `Privacy Badger` évitent les virus, et avoir ses mails depuis `Thunderbird` permet d'avoir ses messages d'avant le piratage disponible

## Vérifier si des comptes à soi ont été piratés

Tapez régulièrement votre adresse mail sur le site [haveIBeenPwned](https://haveibeenpwned.com/). Il listera d'eventuels comptes compromis.

## Avoir un mot de passe solide
Un bon mot de passe doit suivre plusieurs rêgles:

- Il doit faire minimum 12 charactères
- Il ne doit pas contenir de mot
- Il doit contenir plus d'une majuscule
- Il doit contenir plus d'un chiffre
- Il est préférable qu'il soit intelligible/prononçable en français
- Il est préférable qu'il n'ait pas plus de deux fois la même lettre, et que les deux occurences soient éloignées (pour éviter les erreurs)
- Il doit contenir plus d'un caractère spécial (ni lettre; ni chiffre). Les opérateurs mathématiques, parenthèses et tirets sont de bons choix
- Il ne doit pas être noté sur un fichier informatique, que sur du papier (qui a le mérite de ne pas être piratable !)

## Installer Signal Spam sur vos navigateur
`Signal Spam` est un répertoire de spam alimenté par la CNIL, la gendarmerie nationale, le ministère de l'intérieur, de nombreux fournisseurs d'adresses mail, et les internautes.
Elle permet de recouper les informations sur les pirates afin de pouvoir les poursuivre judiciairement.

Le service vous informe quand vous allez sur un site dangereux, quand vous recevez un mail d'une adresse défavorablement connue, mais aussi de raporter des messages indésirables rapidement.

Pour l'utiliser, allez sur [le site officiel](https://www.signal-spam.fr/), crééz un compte [à cette page](https://signalants.signal-spam.fr/sign_up) et téléchargez les extensions navigateur et Thunderbird [ici](https://www.signal-spam.fr/tous-les-modules/).

Quand vous recevrez des spams, vous pourrez désormais les signaler en cliquant sur l'icône "chouette", puis en confirmant.


## En cas d'email suspicieux d'un utilisateur Gmail
Si l'email suspicieux vient d'une addresse inconnue se terminant en `@gmail.com`, rapportez-la à Google pour qu'ils suppriment le compte ou, au cas où le compte aurait été volé, qu'ils aident son propriétaire à en reprendre le contrôle. Cela se fait avec [ce formulaire](https://support.google.com/mail/contact/abuse).

## Prévenir son entourage

Les emails malintentionnés connaissent votre adresse mail de plusieurs façons:

- Si vos proches ont été piratés, et que leur carnet d'adresse a été volé
- Si votre mail est disponible en ligne sur un site internet
- Si un site sur lequel vous avez un compte s'est fait piraté

La sécurité informatique de votre entourage concerne donc la vôtre. Si vous ou un proche vous êtes faits piratés, envoyez cette page à tous vos contacts mails:
```
https://ccleaner.pagesperso-orange.fr/prevention/
```
(Vous pouvez copier l'adresse en cliquant sur l'icône qui apparait à droite du lien)

*[CNIL] : Commission Nationale de l'Informatique et des Libertés 